package net.ofsc.api;


import com.google.gson.Gson;
import net.ofsc.api.rest.Container;
import net.ofsc.api.utils.FloatRandom;
import net.ofsc.api.utils.SettingsSwitcher;
import net.ofsc.core.region.LocationArea;
import net.ofsc.core.region.LocationPing;
import net.ofsc.core.settings.Settings;
import net.ofsc.core.vehicle.Vehicle;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(
        classes = WebService.class)
@AutoConfigureMockMvc
public class LoadTest {

    static {
        SettingsSwitcher.MAX_SETTINGS();
    }

    @Autowired
    private MockMvc mvc;

    private final Logger logger = LoggerFactory.getLogger(LoadTest.class);

    private final Integer vehiclesSize = 100_000;
    private final List<UUID> keyContainer = new ArrayList<>(vehiclesSize);

    @Before
    public void trackingRegion() throws Exception {
        logger.warn("Preparation of 100_000 records has begun, it may take some time.");

        Settings settings = Settings.instance;
        FloatRandom random = new FloatRandom();

        for (int i = 0; i < vehiclesSize; i++) {
            UUID key = UUID.randomUUID();

            mvc.perform(put(
                    registrarNewVehicle(
                            key,
                            random.nextFloat(settings.areaBottomRightLat, settings.areaTopLeftLat),
                            random.nextFloat(settings.areaTopLeftLng, settings.areaBottomRightLng))));

            keyContainer.add(key);
        }

        logger.warn("Preparation successful completed.");
    }

    private String registrarNewVehicle(UUID key, float lat, float lon) {
        return "/vehicles/v1/registration" +
                "/" + key +
                "/location" +
                "/" + lat +
                "/" + lon;
    }


    public void trackingRegion_collect_100K_vehicles() throws Exception {
        logger.info("Select of all available area started.");

        MvcResult result = mvc.perform(
                get(getAreaRequestUrl(Settings.instance.getSupportedArea())))
                .andExpect(status().isOk())
                .andReturn();

        List vResult = new Gson().fromJson(new String(result.getResponse().getContentAsByteArray()), List.class);

        logger.info("Select of all available area completed.Found [" + vResult.size() + "] vehicles");
    }

    private String getAreaRequestUrl(LocationArea area) {
        return "/vehicles/v1/tracking/region" +
                "/" + area.topLeftLat +
                "/" + area.topLeftLng +
                "/" + area.bottomRightLat +
                "/" + area.bottomRightLng;
    }

    @Test
    public void trackingRegion_1_from_100K_vehicles() throws Exception {
        logger.info("Select vehicle for id started.");

        String response = getVehiclesRequestUrl(keyContainer.get(new Random().nextInt(vehiclesSize)));

        MvcResult result = mvc
                .perform(get(response))
                .andExpect(status().isOk())
                .andReturn();

        Vehicle vResult = new Gson().fromJson(new String(result.getResponse().getContentAsByteArray()), Vehicle.class);

        logger.info("Select vehicle completed. " + vResult);
    }

    private String getVehiclesRequestUrl(UUID uuid) {
        return "/vehicles/v1/tracking/location/" + uuid;
    }


}


