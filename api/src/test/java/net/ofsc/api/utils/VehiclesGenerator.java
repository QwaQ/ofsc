package net.ofsc.api.utils;

import net.ofsc.core.vehicle.Vehicle;
import net.ofsc.core.region.LocationArea;
import net.ofsc.core.region.LocationPing;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class VehiclesGenerator {

    public static List<Vehicle> generate(LocationArea inArea, int quantity) {
        List<Vehicle> result = new ArrayList<>(quantity + 1);

        FloatRandom random = new FloatRandom();

        for (int i = 0; i < quantity; i++) {
            Vehicle vehicle = new Vehicle();
            LocationPing location = new LocationPing(
                    random.nextFloat(inArea.bottomRightLat, inArea.topLeftLat),
                    random.nextFloat(inArea.topLeftLng, inArea.bottomRightLng));

            vehicle.updateLocation(location);

            result.add(vehicle);
        }

        return result;
    }

}
