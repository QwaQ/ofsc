package net.ofsc.api.utils;

import java.util.Random;

public class FloatRandom extends Random {

    public float nextFloat(float max,
                           float min) {
        return this.nextFloat() * (max - min) + min;
    }


}
