package net.ofsc.api.rest;

import com.google.gson.Gson;
import net.ofsc.core.region.LocationPing;
import net.ofsc.core.settings.Settings;
import net.ofsc.core.vehicle.Vehicle;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.UUID;


@RunWith(SpringRunner.class)
public class ContainerTest {

    @Autowired
    private Container container;

    @TestConfiguration
    static class ContainerConfiguration {

        @Bean
        public Container containerService() {
            return new Container();
        }

        @Bean
        public VehiclePartitionHolder vehiclesHolderService() {
            return new VehiclePartitionHolder();
        }

        @Bean
        public RegionPartitionHolder regionHolderService() {
            return new RegionPartitionHolder();
        }
    }


    private UUID uuid = UUID.randomUUID();
    private LocationPing start = new LocationPing(0f, 0f);
    private Vehicle vehicle = new Vehicle(uuid, start);

    @Before
    public void setUp() {
        container.registrationVehicle(vehicle);
    }


    @Test
    public void testPingVehicles() {
        Assert.assertTrue(container.getVehicleById(uuid).contains(vehicle.getId().toString()));
    }

    @Test
    public void testUpdateLocationVehicles() {
        LocationPing newLocation = new LocationPing(100f, 100f);
        container.updateVehicleLocation(uuid, newLocation);

        Vehicle result = new Gson().fromJson(container.getVehicleById(uuid), Vehicle.class);

        Assert.assertEquals(result.getLocation(), newLocation);
    }

    @Test
    public void testGetAllVehiclesFrom() {
        String json = container.getAllVehiclesFrom(Settings.instance.getSupportedArea());

        List result = new Gson().fromJson(json, List.class);

        Assert.assertFalse(result.isEmpty());
    }
}