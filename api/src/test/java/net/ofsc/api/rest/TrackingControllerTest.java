package net.ofsc.api.rest;

import com.google.gson.Gson;
import net.ofsc.api.WebService;
import net.ofsc.core.region.LocationArea;
import net.ofsc.core.region.LocationPing;
import net.ofsc.core.settings.Settings;
import net.ofsc.core.vehicle.Vehicle;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.List;
import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(
        classes = WebService.class)
@AutoConfigureMockMvc
public class TrackingControllerTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private Container container;

    @Test
    public void trackingRegion() throws Exception {
        LocationArea area = Settings.instance.getSupportedArea();

        Vehicle v = new Vehicle();

        container.registrationVehicle(v);

        mvc.perform(
                get(getAreaRequestUrl(area)))
                .andExpect(status().isOk());
    }

    @Test
    public void trackingRegion_part() throws Exception {
        LocationArea area = LocationArea.of(
                1500,
                500,
                500,
                1500);

        container.registrationVehicle(UUID.randomUUID(), new LocationPing(0f, 0f));

        container.registrationVehicle(UUID.randomUUID(), new LocationPing(1000f, 1000f));

        MvcResult result = mvc.perform(
                get(getAreaRequestUrl(area)))
                .andExpect(status().isOk())
                .andReturn();

        List resultList = new Gson().fromJson(new String(result.getResponse().getContentAsByteArray()), List.class);

        Assert.assertNotNull(resultList);
    }

    private String getAreaRequestUrl(LocationArea area) {
        return "/vehicles/v1/tracking/region" +
                "/" + area.topLeftLat +
                "/" + area.topLeftLng +
                "/" + area.bottomRightLat +
                "/" + area.bottomRightLng;
    }


    @Test
    public void pingVehicles() throws Exception {
        UUID vehicleID = UUID.randomUUID();
        container.registrationVehicle(vehicleID, new LocationPing(0f, 0f));

        MvcResult result = mvc.perform(
                get(getVehiclesRequestUrl(vehicleID)))
                .andExpect(status().isOk())
                .andReturn();

        Vehicle vehicle = new Gson().fromJson(new String(result.getResponse().getContentAsByteArray()), Vehicle.class);

        Assert.assertNotNull(vehicle);
    }

    private String getVehiclesRequestUrl(UUID uuid) {
        return "/vehicles/v1/tracking/location/" + uuid;
    }



}