package net.ofsc.api.rest;

import net.ofsc.api.utils.SettingsSwitcher;
import net.ofsc.api.utils.VehiclesGenerator;
import net.ofsc.core.region.LocationArea;
import net.ofsc.core.region.LocationPing;
import net.ofsc.core.vehicle.Vehicle;
import net.ofsc.core.settings.Settings;
import net.ofsc.core.utils.RegionPartitionUtils;
import org.testng.Assert;
import org.testng.annotations.Test;

public class RegionPartitionHolderSelectorTest {

    /**
     * Распределение случайным образом транспортных средств
     * поиск по всей зоне
     */
    @Test
    public void testGetAllVehiclesFrom_simple() {
        SettingsSwitcher.SIMPLE_SETTINGS();

        RegionPartitionHolder holder = new RegionPartitionHolder();

        Settings settings = Settings.instance;

        LocationArea centerArea = LocationArea.of(
                settings.areaTopLeftLat,
                settings.areaTopLeftLng,
                settings.areaBottomRightLat,
                settings.areaBottomRightLng);

        int expected_vehicle = 10_000;

        for (Vehicle vehicle : VehiclesGenerator.generate(centerArea, expected_vehicle))
            holder.get(vehicle.getLocation()).registration(vehicle);

        int selected_vehicle = holder.getAllVehiclesFrom(centerArea).get().size();

        Assert.assertEquals(expected_vehicle, selected_vehicle);
    }

    @Test
    public void testGetAllVehiclesFrom_notCompleteMatch() {
        SettingsSwitcher.SIMPLE_SETTINGS();

        RegionPartitionHolder holder = new RegionPartitionHolder();

        Settings settings = Settings.instance;

        LocationArea generationArea = LocationArea.of(
                settings.areaTopLeftLat,
                settings.areaTopLeftLng,
                settings.areaBottomRightLat,
                settings.areaBottomRightLng);

        int expected_vehicle = 10_000;

        for (Vehicle vehicle : VehiclesGenerator.generate(generationArea, expected_vehicle))
            holder.get(vehicle.getLocation()).registration(vehicle);


        float latPartExtension = RegionPartitionUtils.getLatExtension(settings) / 2;
        float lngPartExtension = RegionPartitionUtils.getLngExtension(settings) / 2;

        LocationArea selectionArea = LocationArea.of(
                generationArea.topLeftLat - latPartExtension,
                generationArea.topLeftLng + lngPartExtension,
                generationArea.bottomRightLat + latPartExtension,
                generationArea.bottomRightLng - lngPartExtension);

        int selected_vehicle = holder.getAllVehiclesFrom(selectionArea).get().size();

        Assert.assertNotEquals(expected_vehicle, selected_vehicle);
    }


    @Test
    public void testGetAllVehiclesFrom_SelectedRegionMatch() {
        SettingsSwitcher.SIMPLE_SETTINGS();

        RegionPartitionHolder holder = new RegionPartitionHolder();

        Settings settings = Settings.instance;


        holder.registration(new Vehicle(new LocationPing(10, 10)));
        holder.registration(new Vehicle(new LocationPing(12, 12)));
        holder.registration(new Vehicle(new LocationPing(10, 10)));

        holder.registration(new Vehicle(new LocationPing(1001, 1001)));
        holder.registration(new Vehicle(new LocationPing(1001, 1001)));


        LocationArea selectionArea = LocationArea.of(
                settings.areaTopLeftLat / 2,
                settings.areaTopLeftLng / 2,
                settings.areaBottomRightLat / 2,
                settings.areaBottomRightLng / 2);

        int selected_vehicle = holder.getAllVehiclesFrom(selectionArea).get().size();

        Assert.assertEquals(selected_vehicle, 3);
    }

    @Test
    public void testGetAllVehiclesFrom_SelectedForRegionMatch() {
        SettingsSwitcher.SIMPLE_SETTINGS();
        RegionPartitionHolder holder = new RegionPartitionHolder();

        Settings settings = Settings.instance;


        holder.registration(new Vehicle(new LocationPing(950, 950)));
        holder.registration(new Vehicle(new LocationPing(1050, 1050)));
        holder.registration(new Vehicle(new LocationPing(550, 550)));

        holder.registration(new Vehicle(new LocationPing(1001, 1001)));
        holder.registration(new Vehicle(new LocationPing(950, 1050)));

        //out of select area
        holder.registration(new Vehicle(new LocationPing(10, 10)));

        float latPartExtension = RegionPartitionUtils.getLatExtension(settings) / 2;
        float lngPartExtension = RegionPartitionUtils.getLngExtension(settings) / 2;

        LocationArea selectionArea = LocationArea.of(
                settings.areaTopLeftLat - latPartExtension,
                settings.areaTopLeftLng + lngPartExtension,
                settings.areaBottomRightLat + latPartExtension,
                settings.areaBottomRightLng - lngPartExtension);

        int selected_vehicle = holder.getAllVehiclesFrom(selectionArea).get().size();

        Assert.assertEquals(selected_vehicle, 5);
    }
}