package net.ofsc.api.rest;

import net.ofsc.api.utils.SettingsSwitcher;
import net.ofsc.core.region.LocationArea;
import net.ofsc.core.region.RegionPartitionsGenerator;
import net.ofsc.core.region.RegionSubPartition;
import net.ofsc.core.settings.Settings;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.Map;


public class RegionPartitionsGeneratorTest {

    @Test
    public void testGenerate() {
        SettingsSwitcher.SIMPLE_SETTINGS();
        Map<LocationArea, RegionSubPartition> container = RegionPartitionsGenerator.generate();
        Assert.assertEquals(container.size(), expectedSize());
    }

    @Test
    public void testCheckForUnicPartitions() {
        SettingsSwitcher.LARGE_SETTINGS();
        Map<LocationArea, RegionSubPartition> container = RegionPartitionsGenerator.generate();
        Assert.assertEquals(container.size(), expectedSize());
    }


    private int expectedSize() {
        return (int) Math.pow(Settings.instance.partitionIndex, 2d);
    }
}