package net.ofsc.api.rest;

import net.ofsc.api.utils.SettingsSwitcher;
import net.ofsc.core.region.LocationArea;
import net.ofsc.core.region.LocationPing;
import net.ofsc.core.region.RegionSubPartition;
import org.testng.Assert;
import org.testng.annotations.Test;

public class RegionPartitionHolderGetterTest {

    @Test
    public void successfulTestGetFromSimple() {
        SettingsSwitcher.SIMPLE_SETTINGS();
        RegionPartitionHolder partition = new RegionPartitionHolder();

        LocationArea expected = LocationArea.of(
                1000f,
                0f,
                0f,
                1000L);
        LocationPing point = new LocationPing(10, 10);

        RegionSubPartition subPartition = partition.get(point);

        Assert.assertEquals(
                subPartition.getRange(),
                expected
        );
    }


    @Test
    public void successfulTestGetFromLarge() {
        SettingsSwitcher.LARGE_SETTINGS();

        RegionPartitionHolder partition = new RegionPartitionHolder();
        LocationPing point = new LocationPing(10, 10);

        RegionSubPartition subPartition = partition.get(point);
        subPartition.inRegion(point);

        Assert.assertEquals(
                subPartition.getRange(),
                LocationArea.of(
                        708.65625f,
                        0f,
                        0f,
                        708.66144f)
        );
    }


    @Test
    public void successfulTestGetFromMax() {
        SettingsSwitcher.MAX_SETTINGS();

        RegionPartitionHolder partition = new RegionPartitionHolder();
        LocationPing point = new LocationPing(10, 10);

        RegionSubPartition subPartition = partition.get(point);
        subPartition.inRegion(point);

        Assert.assertEquals(
                subPartition.getRange(),
                LocationArea.of(
                        2.6793874E36f,
                        0f,
                        0f,
                        2.6793878E36f
                )
        );
    }
}