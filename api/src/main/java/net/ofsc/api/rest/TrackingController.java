package net.ofsc.api.rest;

import net.ofsc.api.exceptions.IllegalResponseArgumentException;
import net.ofsc.core.region.LocationArea;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

import static org.springframework.http.HttpStatus.*;

@RestController
public class TrackingController {

    @Autowired
    private Container container;
    private final Logger logger = LoggerFactory.getLogger(TrackingController.class);

    @RequestMapping(
            value = "/vehicles/v1/tracking/region/{top_left_lat}/{top_left_lng}/{bottom_right_lat}/{bottom_right_lng}",
            method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity<String> trackingRegion(
            @PathVariable("top_left_lat") float top_left_lat,
            @PathVariable("top_left_lng") float top_left_lng,
            @PathVariable("bottom_right_lat") float bottom_right_lat,
            @PathVariable("bottom_right_lng") float bottom_right_lng) {

        String body;
        try {
            body = container.getAllVehiclesFrom(LocationArea
                    .of(top_left_lat,
                            top_left_lng,
                            bottom_right_lat,
                            bottom_right_lng));
        } catch (IllegalResponseArgumentException e) {
            return new ResponseEntity<>(e.toString(), BAD_REQUEST);
        } catch (Exception e) {
            return new ResponseEntity<>(e.toString(), INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<>(body, OK);
    }


    @RequestMapping(
            value = "/vehicles/v1/tracking/location/{vehicle-uuid}",
            method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity pingVehicles(@PathVariable("vehicle-uuid") UUID vehicleId) {

        String body;
        try {
            body = container.getVehicleById(vehicleId);
        } catch (Exception e) {
            return new ResponseEntity<>(e.toString(), INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<>(body, OK);
    }
}
