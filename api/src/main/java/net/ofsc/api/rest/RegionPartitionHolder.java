package net.ofsc.api.rest;

import net.ofsc.core.region.LocationArea;
import net.ofsc.core.region.LocationPing;
import net.ofsc.core.region.RegionPartitionsGenerator;
import net.ofsc.core.region.RegionSubPartition;
import net.ofsc.core.vehicle.Vehicle;
import net.ofsc.core.region.consumers.AllVehicleConsumer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;

@Service
@Scope("singleton")
public class RegionPartitionHolder {
    private final Map<LocationArea, RegionSubPartition> partitions;

    private final Logger logger = LoggerFactory.getLogger(RegionPartitionHolder.class);

    public RegionPartitionHolder() {
        logger.info("RegionPartitionHolder initialization  started.");
        this.partitions = RegionPartitionsGenerator.generate();

        if (logger.isDebugEnabled()) {
            logger.debug(describePartitions());
        }

        logger.info("RegionPartitionHolder initialization was successful completed.");
    }

    private String describePartitions() {
        StringBuilder sb = new StringBuilder("<");
        String separator = System.lineSeparator();

        for (Map.Entry<LocationArea, RegionSubPartition> entry : partitions.entrySet()) {
            sb.append("Area: ").append(entry.getKey()).append(separator);
            sb.append("SubPartition: ").append(entry.getValue()).append(separator);
        }

        return sb.append(">").toString();
    }

    public void registration(Vehicle vehicle) {
        this.get(vehicle).registration(vehicle);
    }

    public void unregistration(Vehicle vehicle) {
        this.get(vehicle).registration(vehicle);
    }

    public RegionSubPartition get(Vehicle vehicle) {
        return get(vehicle.getLocation());
    }

    public RegionSubPartition get(LocationPing location) {
        //noinspection SuspiciousMethodCalls
        return partitions.get(location);
    }

    public RegionSubPartition get(LocationArea area) {
        return partitions.get(area);
    }


    public Supplier<List<Vehicle>> getAllVehiclesFrom(LocationArea selectionArea) {
        return new AllVehicleConsumer(this::get, selectionArea);
    }
}
