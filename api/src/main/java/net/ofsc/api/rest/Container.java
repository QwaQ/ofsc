package net.ofsc.api.rest;

import com.google.gson.Gson;
import net.ofsc.api.exceptions.InvalidSelectedAreaException;
import net.ofsc.api.exceptions.InvalidVehicleLocationException;
import net.ofsc.api.exceptions.UnRegistredVehicleException;
import net.ofsc.core.region.LocationArea;
import net.ofsc.core.region.LocationPing;
import net.ofsc.core.region.RegionSubPartition;
import net.ofsc.core.settings.Settings;
import net.ofsc.core.vehicle.Vehicle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.UUID;

@Component
public class Container {

    @Autowired
    private VehiclePartitionHolder vehiclesHolder;
    @Autowired
    private RegionPartitionHolder regionHolder;

    private final Settings settings = Settings.instance;

    private final Logger logger = LoggerFactory.getLogger(Container.class);

    public Container() {
        logger.info("Container staring initialize");

        logger.debug(Settings.instance.toString());

        logger.info("Container staring initialize");
    }

    public String getAllVehiclesFrom(LocationArea selectedArea) throws InvalidSelectedAreaException {
        logger.debug("getAllVehiclesFrom(%s)", selectedArea);
        if (!settings.getSupportedArea().inArea(selectedArea))
            throw new InvalidSelectedAreaException(selectedArea, settings.getSupportedArea());

        List<Vehicle> result = regionHolder
                .getAllVehiclesFrom(selectedArea)
                .get();

        return new Gson().toJson(result);
    }

    public String getVehicleById(UUID uuid) {
        logger.debug("getVehicleById(%s)", uuid);
        Vehicle result = vehiclesHolder
                .getVehicle(uuid)
                .get();

        return new Gson().toJson(result);
    }

    public void registrationVehicle(Vehicle vehicle) {
        logger.debug("registrationVehicle(%s)", vehicle);
        vehiclesHolder.registration(vehicle);

        if (vehicle.getLocation() != null)
            regionHolder.registration(vehicle);
    }

    public void registrationVehicle(UUID uuid) {
        registrationVehicle(new Vehicle(uuid));
    }


    public void registrationVehicle(UUID uuid, LocationPing location) throws InvalidVehicleLocationException {
        logger.debug("registrationVehicle(%s,%s)", uuid, location);
        if (!settings.getSupportedArea().inArea(location))
            throw new InvalidVehicleLocationException(location, settings.getSupportedArea());

        registrationVehicle(new Vehicle(uuid, location));
    }

    public void updateVehicleLocation(UUID uuid, LocationPing newLocation) throws InvalidVehicleLocationException {
        logger.debug("updateVehicleLocation(%s,%s)", uuid, newLocation);
        if (!settings.getSupportedArea().inArea(newLocation))
            throw new InvalidVehicleLocationException(newLocation, settings.getSupportedArea());

        Vehicle vehicle = vehiclesHolder.getVehicle(uuid).get();
        if (vehicle == null)
            throw new UnRegistredVehicleException(uuid);

        RegionSubPartition subPartition = regionHolder.get(vehicle.getLocation());

        //if a new location in a partition range,
        //update location will come cheap
        if (subPartition.inRegion(newLocation))
            vehicle.updateLocation(newLocation);

        else /*a little longer way*/ {
            subPartition.unregistration(vehicle);
            vehicle.updateLocation(newLocation);
            regionHolder.registration(vehicle);
        }
    }
}
