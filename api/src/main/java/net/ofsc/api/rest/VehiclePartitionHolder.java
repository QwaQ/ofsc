package net.ofsc.api.rest;

import net.ofsc.core.exceptions.InvalidPartitioningKeyException;
import net.ofsc.core.vehicle.Vehicle;
import net.ofsc.core.vehicle.consumers.VehicleConsumer;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.function.Supplier;

import static net.ofsc.core.constants.Constants.HASH_MODIFIER;

@Service
@Scope("singleton")
public class VehiclePartitionHolder {
    private final Map<Integer, List<Vehicle>> partitions;

    public VehiclePartitionHolder() {
        this.partitions = new HashMap<>();
        for (int i = 0; i < HASH_MODIFIER; i++) {
            partitions.put(i, new CopyOnWriteArrayList<>());
        }
    }

    public void registration(Vehicle vehicle) {
        this.getPartition(vehicle.getId()).add(vehicle);
    }

    public Supplier<Vehicle> getVehicle(UUID id) {
        return new VehicleConsumer(id, getPartition(id));
    }

    private List<Vehicle> getPartition(UUID id) {
        return getPartition(getPartitionKey(id));
    }

    private List<Vehicle> getPartition(Integer key) {
        if (key < 0 || key > HASH_MODIFIER)
            throw new InvalidPartitioningKeyException(key);

        return partitions.get(key);
    }

    private int getPartitionKey(UUID id) {
        return Math.abs(id.hashCode() % HASH_MODIFIER);
    }
}
