package net.ofsc.api.rest;

import net.ofsc.api.exceptions.IllegalResponseArgumentException;
import net.ofsc.api.exceptions.InvalidSelectedAreaException;
import net.ofsc.core.region.LocationArea;
import net.ofsc.core.region.LocationPing;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

import static org.springframework.http.HttpStatus.*;

@RestController
public class UpdateController {

    @Autowired
    private Container container;
    private final Logger logger = LoggerFactory.getLogger(UpdateController.class);

    @RequestMapping(value = "/vehicles/v1/registration/{uuid}", method = RequestMethod.PUT)
    public @ResponseBody
    ResponseEntity<String> vehiclesRegistration(@PathVariable("uuid") UUID uuid) {
        try {
            container.registrationVehicle(uuid);
        } catch (Exception e) {
            return new ResponseEntity<>(e.toString(), INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<>(OK);
    }

    @RequestMapping(
            value = "/vehicles/v1/registration/{uuid}/location/{lat}/{lon}",
            method = RequestMethod.PUT)
    public @ResponseBody
    ResponseEntity<String> vehiclesRegistrationWithLocation(
            @PathVariable("uuid") UUID uuid,
            @PathVariable("lat") Float lan,
            @PathVariable("lon") Float lot) {
        try {
            container.registrationVehicle(uuid, new LocationPing(lan, lot));
        } catch (IllegalResponseArgumentException ex) {
            return new ResponseEntity<>(ex.toString(), BAD_REQUEST);
        } catch (Exception e) {
            return new ResponseEntity<>(e.toString(), INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<>(OK);
    }


    @RequestMapping(
            value = "/vehicles/v1/update/{uuid}/location/{lan}/{lot}",
            method = RequestMethod.PUT)
    public @ResponseBody
    ResponseEntity updatePosition(
            @PathVariable("uuid") UUID uuid,
            @PathVariable("lan") Float lan,
            @PathVariable("lot") Float lot) {

        try {
            container.updateVehicleLocation(uuid, new LocationPing(lan, lot));
        } catch (Exception e) {
            return new ResponseEntity<>(e.toString(), INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<>(OK);
    }
}
