package net.ofsc.api.exceptions;

import java.util.UUID;

public class UnRegistredVehicleException extends IllegalResponseArgumentException {

    private static final String MSG = "Vehicle [%s] is not found!";

    public UnRegistredVehicleException(UUID uuid) {
        super(String.format(MSG, uuid));

    }
}
