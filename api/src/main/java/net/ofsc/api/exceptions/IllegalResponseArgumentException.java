package net.ofsc.api.exceptions;

public class IllegalResponseArgumentException extends IllegalArgumentException {

    public IllegalResponseArgumentException(String msg) {
        super(msg);
    }
}
