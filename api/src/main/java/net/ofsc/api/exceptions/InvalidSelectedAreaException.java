package net.ofsc.api.exceptions;

import net.ofsc.core.region.LocationArea;

public class InvalidSelectedAreaException extends IllegalResponseArgumentException {

    private static final String MSG = "Selected area is out of battered area!%n" +
            "Slected area:[%s]%n" +
            "battered area:[%s]%n";

    public InvalidSelectedAreaException(LocationArea selectedArea, LocationArea batteredArea) {
        super(String.format(MSG, selectedArea, batteredArea));
    }
}
