package net.ofsc.api.exceptions;

import net.ofsc.core.region.LocationArea;
import net.ofsc.core.region.LocationPing;

public class InvalidVehicleLocationException extends IllegalResponseArgumentException {

    private static final String MSG = "Vehicle location is out of battered area!%n" +
            "Vehicle location:[%s]%n" +
            "battered area:[%s]%n";

    public InvalidVehicleLocationException(LocationPing vehicleLocation, LocationArea batteredArea) {
        super(String.format(MSG, vehicleLocation, batteredArea));

    }
}
