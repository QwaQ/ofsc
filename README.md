The goal of this document is to provide step by step instructions for Building and Running the project.


### Building

1. Call Terminal
2. Switch to the project root directory
3. Call the following command: *mvn clean install*
4. Wait until the project building is completed - this may take some time
5. Switch to the folder "/out". Here you wil find project *ofsc-vehicles-tracking.jar* file

### Running

1. Call Terminal
2. Switch to the directory where project .jar file is located
3. Call the following command: *java -jar ofsc-vehicles-tracking.jar*

After that project is running.

###Available endpoints:

####Tracking endpoints
1. GET:/vehicles/v1/tracking/region/{top_left_lat}/{top_left_lng}/{bottom_right_lat}/{bottom_right_lng}
    Returns the list of vehicles located within a given rectangle
    where: 
        top_left_lat - top latitude
        top_left_lng - top longitude
        bottom_right_lat - bottom latitude
        bottom_right_lng - bottom longitude
        
2. GET:/vehicles/v1/tracking/location/{vehicle-uuid}
    Allows to track location of a given vehicle.
    where:
        vehicle-uuid - is uniq id of a vehicle
        
####Update endpoints

1. PUT:/vehicles/v1/registration/{uuid}/location/{lat}/{lon}
    Registers a new vehicle with the start coordinates
    where:
        uuid - is uniq id of a vehicle
        lat - vehicle start latitude
        lng - vehicle start longitude
        
2. PUT:/vehicles/v1/update/{uuid}/location/{lan}/{lot}
    Update position of a vehicle
    where:
        uuid - is uniq id of a vehicle
        lat -  new vehicle latitude
        lng - new vehicle longitude
        
        
        
        