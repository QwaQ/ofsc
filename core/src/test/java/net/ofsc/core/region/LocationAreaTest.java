package net.ofsc.core.region;

import net.ofsc.core.settings.Settings;
import net.ofsc.core.utils.SettinsSwicher;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class LocationAreaTest {

    LocationArea area = LocationArea.of(
            2000f,
            1000f,
            1000f,
            2000f);

    //region positive cases
    @Test
    public void positiveTestInRange() {
        LocationPing location = new LocationPing(1566f, 1236L);

        assertTrue(area.inArea(location));
    }

    @Test
    public void positiveTestInRange_min() {
        LocationPing location = new LocationPing(1000f, 1000L);

        assertTrue(area.inArea(location));
    }

    @Test
    public void positiveTestInRange_max() {
        LocationPing location = new LocationPing(2000f, 2000L);

        assertTrue(area.inArea(location));
    }

    @Test
    public void positiveTestInLngRange() {
        LocationPing location = new LocationPing(1366f, 1536L);

        assertTrue(area.inLngArea(location));
    }

    @Test
    public void positiveTestInLatRange() {
        LocationPing location = new LocationPing(1222f, 1888L);

        assertTrue(area.inLatArea(location));
    }
    //endregion

    //region negative cases
    @Test
    public void negativeTestInRange_absolute() {
        LocationPing location = new LocationPing(999f, 2001L);

        assertFalse(area.inArea(location));
    }

    @Test
    public void negativeTestInRange_latOutOfRange() {
        LocationPing location = new LocationPing(999f, 1500L);

        assertFalse(area.inArea(location));
    }

    @Test
    public void negativeTestInRange_lngOutOfRange() {
        LocationPing location = new LocationPing(1200f, 2001L);

        assertFalse(area.inArea(location));
    }

    @Test
    public void negativeTestInLngRange() {
        LocationPing location = new LocationPing(100f, 2001L);

        assertFalse(area.inLngArea(location));
    }

    @Test
    public void negativeTestInLatRange() {
        LocationPing location = new LocationPing(100f, 2001L);

        assertFalse(area.inLatArea(location));
    }
    //endregion

    @Test
    public void testInArea() {
        SettinsSwicher.SIMPLE_SETTINGS();

        assertTrue(Settings.instance.getSupportedArea().inArea(area));
    }

    @Test
    public void negativeTestInArea() {
        SettinsSwicher.SIMPLE_SETTINGS();

        assertFalse(area.inArea(Settings.instance.getSupportedArea()));
    }
}