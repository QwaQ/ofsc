package net.ofsc.core.utils;

import net.ofsc.core.region.LocationArea;
import net.ofsc.core.settings.Settings;

public class SettinsSwicher {

    public static void SIMPLE_SETTINGS() {
        Settings settings = Settings.instance;
        settings.restToDefault();

        settings.areaTopLeftLat = 2000f;
        settings.areaTopLeftLng = 0f;
        settings.areaBottomRightLat = 0f;
        settings.areaBottomRightLng = 2000f;

        settings.partitionIndex = 2;
    }

    public static void LARGE_SETTINGS() {
        Settings settings = Settings.instance;
        settings.restToDefault();

        settings.areaTopLeftLat = 90000f;
        settings.areaTopLeftLng = 0;
        settings.areaBottomRightLat = 0;
        settings.areaBottomRightLng = 90000f;

        settings.partitionIndex = Byte.MAX_VALUE;
    }


    public static void MAX_SETTINGS(){
        Settings settings = Settings.instance;
        settings.restToDefault();

        settings.areaTopLeftLat = Float.MAX_VALUE - 1E32f;
        settings.areaTopLeftLng = 0;
        settings.areaBottomRightLat = 0;
        settings.areaBottomRightLng = Float.MAX_VALUE - 1E32f;

        settings.partitionIndex = Byte.MAX_VALUE;
    }
}
