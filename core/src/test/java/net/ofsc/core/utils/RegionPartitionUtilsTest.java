package net.ofsc.core.utils;

import net.ofsc.core.region.LocationArea;
import net.ofsc.core.settings.Settings;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;

public class RegionPartitionUtilsTest {

    {
        SettinsSwicher.SIMPLE_SETTINGS();
    }

    private final Settings settings = Settings.instance;

    private final LocationArea area = LocationArea.of(
            settings.areaTopLeftLat,
            settings.areaTopLeftLng,
            settings.areaBottomRightLat,
            settings.areaBottomRightLng);

    @Test
    public void testGetLngExtension() {
        Float result = RegionPartitionUtils.getLngExtension(settings);
        Assert.assertEquals(1000f, result);
    }

    @Test
    public void testGetLatExtension() {
        Float result = RegionPartitionUtils.getLatExtension(settings);
        Assert.assertEquals(1000f, result);
    }

    @Test
    public void testGetLngLength() {
        Float result = RegionPartitionUtils.getLngLength(settings.areaTopLeftLng, settings.areaBottomRightLng);
        Assert.assertEquals(2000f, result);
    }

    @Test
    public void testGetLatLength() {
        Float result = RegionPartitionUtils.getLatLength(settings.areaBottomRightLat, settings.areaTopLeftLat);
        Assert.assertEquals(2000f, result);
    }

    @Test
    public void testLeadToPartitionGrid() {
        List<LocationArea> areas = RegionPartitionUtils.leadToPartitionGrid(area);
        Assert.assertEquals(4, areas.size());
    }

    @Test
    public void testLeadToPartitionGrid_SelectedOneSubRegion() {
        LocationArea selectedArea = LocationArea.of(
                settings.areaTopLeftLat / 2f,
                settings.areaTopLeftLng / 2f,
                settings.areaBottomRightLat / 2f,
                settings.areaBottomRightLng / 2f);

        List<LocationArea> areas = RegionPartitionUtils.leadToPartitionGrid(selectedArea);
        Assert.assertEquals(areas.size(), 1);
    }

    @Test
    public void testLeadToPartitionGrid_SelectedTwoSubRegion() {
        LocationArea selectedArea = LocationArea.of(
                settings.areaTopLeftLat / 2f,
                settings.areaTopLeftLng,
                settings.areaBottomRightLat / 2f,
                settings.areaBottomRightLng);

        List<LocationArea> areas = RegionPartitionUtils.leadToPartitionGrid(selectedArea);
        Assert.assertEquals(areas.size(), 2);
    }

    @Test
    public void testLeadToPartitionGrid_SelectedCenterSubRegion() {
        float latPartExtension = RegionPartitionUtils.getLatExtension(settings) / 2;
        float lngPartExtension = RegionPartitionUtils.getLngExtension(settings) / 2;

        LocationArea selectedArea = LocationArea.of(
                settings.areaTopLeftLat - latPartExtension,
                settings.areaTopLeftLng + lngPartExtension,
                settings.areaBottomRightLat + latPartExtension,
                settings.areaBottomRightLng - lngPartExtension);

        List<LocationArea> areas = RegionPartitionUtils.leadToPartitionGrid(selectedArea);
        Assert.assertEquals(areas.size(), 4);
    }

    @Test
    public void testLeadToPartitionGrid_SelectedOutOFArea() {
        float latPartExtension = RegionPartitionUtils.getLatExtension(settings) / 2;
        float lngPartExtension = RegionPartitionUtils.getLngExtension(settings) / 2;

        LocationArea selectedArea = LocationArea.of(
                settings.areaTopLeftLat - latPartExtension,
                settings.areaTopLeftLng - lngPartExtension,
                settings.areaBottomRightLat - latPartExtension,
                settings.areaBottomRightLng - lngPartExtension);

        List<LocationArea> areas = RegionPartitionUtils.leadToPartitionGrid(selectedArea);
        Assert.assertEquals(areas.size(), 9);
    }

    @Test
    public void testLeadToPartitionGrid_SelectedAreaLessThanPartitionCellSize() {

        LocationArea selectedArea = LocationArea.of(
                200,
                100,
                100,
                200);

        List<LocationArea> areas = RegionPartitionUtils.leadToPartitionGrid(selectedArea);
        Assert.assertEquals(areas.size(), 1);
    }

    @Test
    public void testLeadToPartitionGrid_SelectedAreaLessThanPartitionCellSize2() {

        LocationArea selectedArea = LocationArea.of(
                200,
                0,
                0,
                200);

        List<LocationArea> areas = RegionPartitionUtils.leadToPartitionGrid(selectedArea);
        Assert.assertEquals(areas.size(), 1);
    }

    @Test
    public void testCalculateLngStateBottom() {
        float latExtension = RegionPartitionUtils.getLngExtension(settings);
        Float state = RegionPartitionUtils.calculateLngStateBottom(settings, 0, latExtension);
        Assert.assertEquals(state, 1000f);
    }

    @Test
    public void testCalculateLngStateTop() {
        float lngExtension = RegionPartitionUtils.getLngExtension(settings);
        Float state = RegionPartitionUtils.calculateLngStateTop(settings, 1, lngExtension);
        Assert.assertEquals(state, 1000f);
    }

    @Test
    public void testCalculateLatStateBottom() {
        float lngExtension = RegionPartitionUtils.getLatExtension(settings);
        Float result = RegionPartitionUtils.calculateLatStateBottom(settings, 1, lngExtension);
        Assert.assertEquals(result, 0f);
    }

    @Test
    public void testCalculateLatStateTop() {
        float latExtension = RegionPartitionUtils.getLatExtension(settings);
        Float result = RegionPartitionUtils.calculateLatStateTop(settings, 0, latExtension);
        Assert.assertEquals(result, 2000f);
    }
}