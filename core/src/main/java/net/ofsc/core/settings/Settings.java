package net.ofsc.core.settings;

import net.ofsc.core.region.LocationArea;

import java.util.concurrent.TimeUnit;

public class Settings {

    public static final Settings instance = new Settings();

    protected Settings() {/*stub*/}

    //region geographical area
    public float areaTopLeftLat = /*default value*/ 2000f;
    public float areaTopLeftLng = /*default value*/ 0f;
    public float areaBottomRightLat = /*default value*/ 0f;
    public float areaBottomRightLng = /*default value*/ 2000f;
    //endregion

    public byte partitionIndex = 10;

    /**
     * Confidence time of vehicle registration.
     * Default value is 30 Seconds
     */
    public Long registrationTime = TimeUnit.MILLISECONDS.convert(30L, TimeUnit.SECONDS);


    public LocationArea getSupportedArea(){
        return LocationArea.of(
                areaTopLeftLat,
                areaTopLeftLng,
                areaBottomRightLat,
                areaBottomRightLng);
    }

    public Settings restToDefault() {
        this.areaTopLeftLat = /*default value*/ 2000f;
        this.areaTopLeftLng = /*default value*/ 0f;
        this.areaBottomRightLat = /*default value*/ 0f;
        this.areaBottomRightLng = /*default value*/ 2000f;

        this.partitionIndex = 10;
        this.registrationTime = TimeUnit.MILLISECONDS.convert(30L, TimeUnit.SECONDS);

        return this;
    }


    @Override
    public String toString() {
        return "Settings{" +
                "areaTopLeftLat=" + areaTopLeftLat +
                ", areaTopLeftLng=" + areaTopLeftLng +
                ", areaBottomRightLat=" + areaBottomRightLat +
                ", areaBottomRightLng=" + areaBottomRightLng +
                ", partitionIndex=" + partitionIndex +
                ", registrationTime=" + registrationTime +
                '}';
    }
}
