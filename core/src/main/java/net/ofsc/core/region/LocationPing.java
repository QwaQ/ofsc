package net.ofsc.core.region;

import java.util.Objects;

public class LocationPing implements Comparable<LocationArea> {
    public final long pingTime;
    public final float latitude;
    public final float longitude;

    public LocationPing(float latitude, float longitude) {
        this.pingTime = System.currentTimeMillis();
        this.latitude = latitude;
        this.longitude = longitude;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof LocationPing)) return false;
        LocationPing location = (LocationPing) o;
        return latitude == location.latitude &&
                longitude == location.longitude;
    }

    @Override
    public String toString() {
        return "LocationPing{" +
                "pingTime=" + pingTime +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                '}';
    }

    @Override
    public int hashCode() {
        return Objects.hash(latitude, longitude);
    }

    @Override
    public int compareTo(LocationArea o) {
        if (o.inArea(this))
            return 0;

        if (latitude > o.topLeftLat)
            return Double.compare(latitude, o.topLeftLat);
        if (longitude < o.topLeftLng)
            return Double.compare(longitude, o.topLeftLng);

        if (latitude < o.bottomRightLat)
            return Double.compare(latitude, o.bottomRightLat);
        if (longitude > o.bottomRightLng)
            return Double.compare(longitude, o.bottomRightLng);

        throw new UnsupportedOperationException("Orientation failed");
    }
}
