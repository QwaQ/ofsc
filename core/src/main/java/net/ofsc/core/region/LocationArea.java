package net.ofsc.core.region;

import com.sun.istack.internal.NotNull;
import net.ofsc.core.exceptions.UnValidLocationRangeException;

import java.util.Objects;

public class LocationArea implements Comparable<LocationArea> {

    //region fields
    public final float topLeftLat;
    public final float topLeftLng;
    public final float bottomRightLat;
    public final float bottomRightLng;
    //endregion

    //region generation
    private LocationArea(float topLeftLat,
                         float topLeftLng,
                         float bottomRightLat,
                         float bottomRightLng) {
        this.topLeftLat = topLeftLat;
        this.topLeftLng = topLeftLng;
        this.bottomRightLat = bottomRightLat;
        this.bottomRightLng = bottomRightLng;
    }

    public static LocationArea of(float topLeftLat,
                                  float topLeftLng,
                                  float bottomRightLat,
                                  float bottomRightLng) {
        if (unValidValue(topLeftLat, bottomRightLat) || unValidValue(bottomRightLng, topLeftLng))
            throw new UnValidLocationRangeException(
                    topLeftLat,
                    topLeftLng,
                    bottomRightLat,
                    bottomRightLng);


        return new LocationArea(topLeftLat, topLeftLng, bottomRightLat, bottomRightLng);
    }

    private static boolean unValidValue(double top, double bottom) {
        return top >= Float.MAX_VALUE || bottom >= Float.MAX_VALUE || top <= bottom;
    }
    //endregion


    //region location matching

    /**
     * Method return true if the dot belongs to the current area
     *
     * @param location
     * @return
     */
    public boolean inArea(@NotNull LocationPing location) {
        return inLatArea(location) && inLngArea(location);
    }

    /*package-private*/ boolean inLngArea(@NotNull LocationPing location) {
        return topLeftLng <= location.longitude
                &&
                location.longitude <= bottomRightLng;
    }

    /*package-private*/ boolean inLatArea(@NotNull LocationPing location) {
        return bottomRightLat <= location.latitude
                &&
                location.latitude <= topLeftLat;
    }
    //endregion

    //region location matching

    /**
     * This method returns true if there all points of the target area belong to the current area.
     *
     * @param area target area
     * @return true if all points of the target area belong to the current
     */
    public boolean inArea(@NotNull LocationArea area) {
        return inLatArea(area) && inLngArea(area);
    }


    /*package-private*/ boolean inLngArea(@NotNull LocationArea location) {
        return topLeftLng <= location.topLeftLat
                &&
                location.bottomRightLng <= bottomRightLng;
    }

    /*package-private*/ boolean inLatArea(@NotNull LocationArea location) {
        return bottomRightLat <= location.bottomRightLat
                &&
                location.topLeftLat <= topLeftLat;
    }

    /**
     * This method returns true if the target area partially overlaps the current one.
     *
     * @param area target area
     * @return true if there are exist common points to the target and the current area.
     */
    public boolean intersectArea(@NotNull LocationArea area) {
        return intersectTopLng(area)
                || intersectBottomLng(area)
                || intersectTopLat(area)
                || intersectBottomLat(area);
    }

    /*package-private*/ boolean intersectTopLng(@NotNull LocationArea location) {
        return topLeftLng <= location.topLeftLat
                &&
                location.topLeftLat <= bottomRightLng;
    }

    /*package-private*/ boolean intersectBottomLng(@NotNull LocationArea location) {
        return topLeftLng <= location.bottomRightLng
                &&
                location.bottomRightLng <= bottomRightLng;
    }

    /*package-private*/ boolean intersectTopLat(@NotNull LocationArea location) {
        return bottomRightLat <= location.topLeftLat
                &&
                location.topLeftLat <= topLeftLat;
    }

    /*package-private*/ boolean intersectBottomLat(@NotNull LocationArea location) {
        return bottomRightLat <= location.bottomRightLat
                &&
                location.bottomRightLat <= topLeftLat;
    }
    //endregion

    @Override
    public int compareTo(LocationArea o) {
        if (this == o)
            return 0;

        int weight = compareOverlap(topLeftLat, o.topLeftLat);

        if (weight != 0)
            return weight;

        weight = compareOverlap(topLeftLng, o.topLeftLng);

        if (weight != 0)
            return weight;

        weight = compareOverlap(bottomRightLat, o.bottomRightLat);

        if (weight != 0)
            return weight;

        return compareOverlap(bottomRightLng, o.bottomRightLng);
    }

    private int compareOverlap(double current, double external) {
        return Double.compare(current, external);
    }

    @Override
    public String toString() {
        return "LocationArea{" +
                "topLeftLat=" + topLeftLat +
                ", topLeftLng=" + topLeftLng +
                ", bottomRightLat=" + bottomRightLat +
                ", bottomRightLng=" + bottomRightLng +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof LocationArea)) return false;
        LocationArea range = (LocationArea) o;
        return Double.compare(range.topLeftLat, topLeftLat) == 0 &&
                Double.compare(range.topLeftLng, topLeftLng) == 0 &&
                Double.compare(range.bottomRightLat, bottomRightLat) == 0 &&
                Double.compare(range.bottomRightLng, bottomRightLng) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(topLeftLat, topLeftLng, bottomRightLat, bottomRightLng);
    }
}
