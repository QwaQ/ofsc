package net.ofsc.core.region;

import net.ofsc.core.settings.Settings;

import java.util.Map;
import java.util.TreeMap;

import static net.ofsc.core.utils.RegionPartitionUtils.*;

/*package-private*/ public class RegionPartitionsGenerator {

    /*package-private*/
    public static Map<LocationArea, RegionSubPartition> generate() {
        Settings settings = Settings.instance;

        /*it could be calculated dynamically*/
        int partitionIndex = settings.partitionIndex;

        float latExtension = getLatExtension(settings);
        float lngExtension = getLngExtension(settings);

        Map<LocationArea, RegionSubPartition> result = new TreeMap<>();

        for (int x = 0; x < partitionIndex; x++) {
            for (int y = 0; y < partitionIndex; y++) {
                putPartition(result,
                        calculateLatStateTop(settings, x, latExtension),
                        calculateLngStateTop(settings, y, lngExtension),
                        calculateLatStateBottom(settings, x, latExtension),
                        calculateLngStateBottom(settings, y, latExtension));
            }
        }

        return result;
    }


    private static void putPartition(Map<LocationArea, RegionSubPartition> result,
                                     float latStateTop,
                                     float lngStateTop,
                                     float latStateBottom,
                                     float lngStateBottom) {
        LocationArea range = LocationArea.of(
                latStateTop,
                lngStateTop,
                latStateBottom,
                lngStateBottom);
        result.put(range, new RegionSubPartition(range));
    }
}
