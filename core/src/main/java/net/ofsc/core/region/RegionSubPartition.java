package net.ofsc.core.region;

import com.sun.istack.internal.NotNull;
import net.ofsc.core.vehicle.Vehicle;
import net.ofsc.core.region.predicates.AliveRegistrationPredicate;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Stream;

public class RegionSubPartition {
    private final LocationArea range;
    private final List<Vehicle> registered;

    public RegionSubPartition(LocationArea range) {
        this.range = range;
        this.registered = new ArrayList<>();
    }

    public LocationArea getRange() {
        return range;
    }

    //region registration
    public boolean inRegion(Vehicle vehicle) {
        return inRegion(vehicle.getLocation());
    }

    public boolean inRegion(LocationPing location) {
        return range.inArea(location);
    }

    public boolean registration(Vehicle vehicle) {
        return registered.add(vehicle);
    }

    public boolean unregistration(Vehicle vehicle) {
        return registered.remove(vehicle);
    }
    //endregion

    //region participants manipulation
    public Stream<Vehicle> getAliveRegistered() {
        return getRegistered(new AliveRegistrationPredicate());
    }


    public Stream<Vehicle> getRegistered(@NotNull Predicate<Vehicle> predicate) {
        return registered.stream().filter(predicate);
    }

    public Stream<Vehicle> getAllRegistered() {
        return registered.stream();
    }

    public List<Vehicle> getAllVehicleAsList() {
        return registered;
    }
    //endregion

    @Override
    public String toString() {
        return "SubPartition{" +
                "range=" + range +
                ", size=" + registered.size() + '}';
    }
}
