package net.ofsc.core.region.consumers;

import net.ofsc.core.settings.Settings;
import net.ofsc.core.vehicle.Vehicle;
import net.ofsc.core.region.LocationArea;
import net.ofsc.core.region.RegionSubPartition;
import net.ofsc.core.region.predicates.AreaContainsPredicate;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import static net.ofsc.core.utils.RegionPartitionUtils.leadToPartitionGrid;

public class AllVehicleConsumer implements Supplier<List<Vehicle>> {

    private final Function<LocationArea, RegionSubPartition> partitionHolder;
    private final LocationArea selectionArea;

    private final AreaContainsPredicate areaContainsPredicate;

    public AllVehicleConsumer(Function<LocationArea, RegionSubPartition> partitionHolder,
                              LocationArea selectionArea) {
        this.partitionHolder = partitionHolder;
        this.selectionArea = selectionArea;

        this.areaContainsPredicate = new AreaContainsPredicate(selectionArea);
    }

    @Override
    public List<Vehicle> get() {
        List<LocationArea> keys = leadToPartitionGrid(selectionArea);

        List<Vehicle> result = new ArrayList<>();

        for (LocationArea key : keys) {
            RegionSubPartition subPartition = partitionHolder.apply(key);

            //fast collect flow
            if (selectionArea.inArea(key)) {
                result.addAll(
                        subPartition.getAllVehicleAsList());
            } else {
                result.addAll(
                        subPartition
                                .getRegistered(areaContainsPredicate)
                                .collect(Collectors.toList()));
            }
        }

        return result;
    }
}
