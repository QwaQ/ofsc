package net.ofsc.core.region.predicates;

import net.ofsc.core.vehicle.Vehicle;
import net.ofsc.core.settings.Settings;

import java.util.function.Predicate;

public class AliveRegistrationPredicate implements Predicate<Vehicle> {

    private final Settings settings = Settings.instance;

    @Override
    public boolean test(Vehicle vehicle) {
        return (System.currentTimeMillis() - vehicle.getRegistrationTime())
                >= settings.registrationTime;
    }
}
