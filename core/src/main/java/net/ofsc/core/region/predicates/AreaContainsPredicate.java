package net.ofsc.core.region.predicates;

import net.ofsc.core.vehicle.Vehicle;
import net.ofsc.core.region.LocationArea;

import java.util.function.Predicate;

public class AreaContainsPredicate implements Predicate<Vehicle> {

    private final LocationArea area;

    public AreaContainsPredicate(LocationArea area) {
        this.area = area;
    }

    @Override
    public boolean test(Vehicle vehicle) {
        return area.inArea(vehicle.getLocation());
    }
}
