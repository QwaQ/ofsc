package net.ofsc.core.utils;

import net.ofsc.core.region.LocationArea;
import net.ofsc.core.settings.Settings;

import java.util.ArrayList;
import java.util.List;

public class RegionPartitionUtils {

    public static float getLngExtension(Settings settings) {
        return getLngLength(settings.areaTopLeftLng, settings.areaBottomRightLng)
                / settings.partitionIndex;
    }

    public static float getLatExtension(Settings settings) {
        return getLatLength(settings.areaBottomRightLat, settings.areaTopLeftLat)
                / settings.partitionIndex;
    }

    public static float getLngLength(float top, float bottom) {
        return Math.abs(top - bottom);
    }

    public static float getLatLength(float bottom, float top) {
        return Math.abs(bottom - top);
    }


    /**
     * Retrieving keys for accelerated sampling.
     *
     * @param area     selected area
     * @return
     */
    public static List<LocationArea> leadToPartitionGrid( LocationArea area) {
        final Settings settings = Settings.instance;

        //calculating a size of the grid partitions
        float latExtension = getLatExtension(settings);
        float lngExtension = getLngExtension(settings);

        //calculating cells that do not fall into the selected area
        float latOffset = (Math.abs(area.topLeftLat - settings.areaTopLeftLat) / latExtension);

        //calculating the length of the edge of the selected area
        float latPartLength = getLatLength(area.bottomRightLat, area.topLeftLat);

        //calculating the number of cells in the grid that covered by the selection area
        int latRange = getRange(latOffset, latPartLength, latExtension);

        //the same operation for another coordinates
        float lngOffset = (Math.abs(area.topLeftLng - settings.areaTopLeftLng) / latExtension);
        float lngPartLength = getLngLength(area.topLeftLng, area.bottomRightLng);
        int lngRange = getRange(lngOffset, lngPartLength, latExtension);


        List<LocationArea> result = new ArrayList<>();

        for (int x = (int) latOffset; x < latRange; x++) {
            for (int y = (int) lngOffset; y < lngRange; y++) {
                result.add(LocationArea.of(
                        calculateLatStateTop(settings, x, latExtension),
                        calculateLngStateTop(settings, y, lngExtension),
                        calculateLatStateBottom(settings, x, latExtension),
                        calculateLngStateBottom(settings, y, latExtension)));
            }
        }

        return result;
    }

    private static int getRange(float offset,
                                float partLength,
                                float extension) {
        if (partLength < extension)
            if (offset != 0f)
                return (int) Math.ceil(offset);
            else
                return 1;

        return (int) Math.ceil(offset + (partLength / extension));
    }

    //region coordinates calculating
    public static float calculateLngStateBottom(Settings settings, int lngIndex, float lngExtension) {
        if (lngIndex == settings.partitionIndex - 1)
            return settings.areaBottomRightLng;
        else
            return settings.areaTopLeftLng + ((lngIndex + 1) * lngExtension);
    }

    public static float calculateLngStateTop(Settings settings, int lngIndex, float lngExtension) {
        //get all rest
        return settings.areaTopLeftLng + (lngIndex * lngExtension);
    }

    public static float calculateLatStateBottom(Settings settings, int latIndex, float latExtension) {
        //get all rest
        if (latIndex == settings.partitionIndex - 1)
            return settings.areaBottomRightLat;
        else
            return settings.areaTopLeftLat - ((latIndex + 1) * latExtension);
    }

    public static float calculateLatStateTop(Settings settings, int latIndex, float latExtension) {
        return settings.areaTopLeftLat - ((latIndex) * latExtension);
    }
    //endregion

}
