package net.ofsc.core.exceptions;

import net.ofsc.core.constants.Constants;

public class InvalidPartitioningKeyException extends IllegalArgumentException {

    public InvalidPartitioningKeyException(Integer partiotnKey) {
        super(String.format(
                "[%d] is Invalid partition key! Valid range is within [%d]-[%d]",
                partiotnKey, 0, Constants.HASH_MODIFIER));

    }
}
