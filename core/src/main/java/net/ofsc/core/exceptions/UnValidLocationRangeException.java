package net.ofsc.core.exceptions;

public class UnValidLocationRangeException extends IllegalArgumentException {

    private final static String MSG =
            "UnValid location range" +
                    "%ntopLeftLat[%f]" +
                    "%ntopLeftLng[%f]" +
                    "%nbottomRightLat[%f]" +
                    "%nbottomRightLng[%f]";

    public UnValidLocationRangeException(double topLeftLat,
                                         double topLeftLng,
                                         double bottomRightLat,
                                         double bottomRightLng) {
        super(String.format(MSG, topLeftLat, topLeftLng, bottomRightLat, bottomRightLng));
    }
}
