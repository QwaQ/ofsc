package net.ofsc.core.vehicle;

import net.ofsc.core.region.LocationPing;

import java.util.Objects;
import java.util.UUID;

public class Vehicle {
    private final UUID id;
    private final long registrationTime;

    private LocationPing location;

    public Vehicle() {
        this.id = UUID.randomUUID();
        this.registrationTime = System.currentTimeMillis();
    }

    public Vehicle(UUID uuid) {
        this.id = uuid;
        this.registrationTime = System.currentTimeMillis();
    }

    public Vehicle(UUID uuid, LocationPing startLocation) {
        this.id = uuid;
        this.location = startLocation;
        this.registrationTime = System.currentTimeMillis();
    }


    public Vehicle(UUID id, Long registrationTime, LocationPing locationPing) {
        this.id = id;
        this.registrationTime = registrationTime;
        this.location = locationPing;
    }

    public Vehicle(LocationPing startLocation) {
        this.id = UUID.randomUUID();
        this.location = startLocation;
        this.registrationTime = System.currentTimeMillis();
    }

    public UUID getId() {
        return id;
    }

    public long getRegistrationTime() {
        return registrationTime;
    }

    public LocationPing getLocation() {
        return location;
    }


    public void updateLocation(LocationPing newLocation) {
        this.location = newLocation;
    }

    @Override
    public String toString() {
        return "Vehicle{" +
                "id=" + id +
                ", registrationTime=" + registrationTime +
                ", location=" + location +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Vehicle)) return false;
        Vehicle vehicle = (Vehicle) o;
        return id.equals(vehicle.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
