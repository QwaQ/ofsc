package net.ofsc.core.vehicle.consumers;

import net.ofsc.core.vehicle.Vehicle;

import java.util.List;
import java.util.UUID;
import java.util.function.Supplier;

public class VehicleConsumer implements Supplier<Vehicle> {

    private final UUID uuid;
    private final List<Vehicle> container;

    public VehicleConsumer(UUID uuid,
                           List<Vehicle> container) {

        this.uuid = uuid;
        this.container = container;
    }

    @Override
    public Vehicle get() {
        for (Vehicle candidate : container)
            if (candidate.getId().equals(uuid))
                return candidate;

        throw new UnsupportedOperationException("Vehicle [" + uuid + "] was not found!");
    }
}
